package com.yjs.cloud.learning.auth.biz.sso.mapper;

import com.yjs.cloud.learning.auth.biz.sso.entity.SsoClient;
import com.yjs.cloud.learning.auth.common.mapper.IBaseMapper;

/**
 * mapper
 *
 * @author Bill.lai
 * @since 2021/6/2
 */
public interface SsoClientMapper extends IBaseMapper<SsoClient> {
}
