package com.yjs.cloud.learning.learn.biz.lesson.mapper;

import com.yjs.cloud.learning.learn.biz.lesson.entity.LessonTargetDepartment;
import com.yjs.cloud.learning.learn.biz.lesson.entity.LessonTargetMember;
import com.yjs.cloud.learning.learn.common.mapper.IBaseMapper;

/**
 * @author Bill.lai
 * @since 2021/6/1
 */
public interface LessonTargetDepartmentMapper extends IBaseMapper<LessonTargetDepartment> {
}
