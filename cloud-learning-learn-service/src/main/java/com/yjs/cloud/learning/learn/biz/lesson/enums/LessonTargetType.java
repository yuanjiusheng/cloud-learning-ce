package com.yjs.cloud.learning.learn.biz.lesson.enums;

/**
 * 目标类型
 *
 * @author Bill.lai
 * @since 2021/6/1
 */
public enum LessonTargetType {
    /**
     * 全部
     */
    all,
    /**
     * 指定
     */
    assign
}
