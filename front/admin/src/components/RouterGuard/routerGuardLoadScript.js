import {checkAuthorities} from "../../util/authorityUtils"

export default {
  inserted(el, binding) {
    const { value } = binding
    if (value && value instanceof Array && value.length > 0) {
      const hasAuthority = checkAuthorities(value)
      if (!hasAuthority) {
        el.parentNode && el.parentNode.removeChild(el)
      }
    } else {
      throw new Error("need authorities! Like v-authority=\"['a1', 'a2']\"");
    }
  }
}
