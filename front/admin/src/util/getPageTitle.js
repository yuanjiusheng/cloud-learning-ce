const vueConfig = require("../../vue.config.js")

export default function getPageTitle(pageTitle) {
  const title = vueConfig.configureWebpack.name
  if (pageTitle) {
    return `${pageTitle} | ${title}`
  }
  return `${title}`
}
