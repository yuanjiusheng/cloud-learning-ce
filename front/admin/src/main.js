import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import "./router/guard";
import store from "./store";
import elementPlus from "./plugins/element/element";

export default createApp(App)
  .use(store)
  .use(router)
  .use(elementPlus)
  .mount("#app");
