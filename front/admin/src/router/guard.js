import {checkAuthorities, filterAsyncRoutes, getUserAuthorities, setRoutes} from "../util/authorityUtils";
import {getToken, refreshToken, removeToken} from "../util/tokenUtils";
import {getUser} from "../util/userUtils";
import {error} from "../util/tipsUtils";
import router, {routes, asyncRoutes} from "./index";
import Index from "../views/home/Index"
import getPageTitle from "../util/getPageTitle";

// 白名单
const whiteList = ["/login", "/work-we-chat", "/ding-talk"];
const goto = (path, next) => {
  next({ path: path, replace: true })
}
// 选择路径
const switchPath = next => {
  if (1 === parseInt("1")) {
    goto("/index", next)
  } else
  if (checkAuthorities(["member_list"])) {
    goto("/member/list", next)
  } else if (checkAuthorities(["member_level"])) {
    goto("/member/level", next)
  } else if (checkAuthorities(["live_channel"])) {
    goto("/live/channel", next)
  } else if (checkAuthorities(["live_category"])) {
    goto("/live/category", next)
  } else if (checkAuthorities(["learning_list"])) {
    goto("/learning/lesson", next)
  } else if (checkAuthorities(["learning_category"])) {
    goto("/learning/category", next)
  } else if (checkAuthorities(["exam_question_lib"])) {
    goto("/exam/question-lib/list", next)
  } else if (checkAuthorities(["exam_question_lib_category"])) {
    goto("/exam/question-lib/category", next)
  } else if (checkAuthorities(["exam_page_list"])) {
    goto("/exam/paper/list", next)
  } else if (checkAuthorities(["exam_category"])) {
    goto("/exam/category", next)
  } else if (checkAuthorities(["exam_list"])) {
    goto("/exam/list", next)
  } else if (checkAuthorities(["exam_mark"])) {
    goto("/exam/mark", next)
  } else if (checkAuthorities(["exam_mock"])) {
    goto("/exam/mock", next)
  } else if (checkAuthorities(["organizational_user"])) {
    goto("/organizational/user", next)
  } else if (checkAuthorities(["authority_role"])) {
    goto("/authority/role", next)
  } else if (checkAuthorities(["authority_authority"])) {
    goto("/authority/authority", next)
  } else if (checkAuthorities(["setting_carousel"])) {
    goto("/setting/carousel", next)
  } else {
    goto("/index", next)
  }
}
const addDynamicRoute = async () => {
  const authorities = await getUserAuthorities();
  if (!authorities || !authorities.length) {
    error("没有权限");
    return false;
  }
  const accessRoutes = filterAsyncRoutes(asyncRoutes, authorities);
  accessRoutes.push({name: "dynamicRoute", path: "/dynamic-route", component: Index, hidden: true})
  const routeList = routes.concat(accessRoutes)
  // 添加路由
  for (const accessedRoute of routeList) {
    await router.addRoute(accessedRoute);
  }
  setRoutes(routeList)
  return true;
}
router.beforeEach(async (to, from, next) => {
  document.title = getPageTitle(to.meta.title)
  refreshToken()
  const hasToken = getToken()
  if (hasToken) {
    if (to.path === "/login") {
      next({ path: "/index" })
    } else {
      const userInfo = getUser()
      if (userInfo) {
        if (to.path === "/") {
          switchPath(next)
        } else if (to.path === "/login") {
          next();
        } else {
          // 没有动态路由
          if (!router.hasRoute("dynamicRoute")) {
            const flag = await addDynamicRoute();
            if (!flag) {
              return;
            }
            next({ ...to, replace: true });
          } else {
            next();
          }
        }
      } else {
        try {
          // 动态路由
          const flag = await addDynamicRoute();
          if (!flag) { return;}
          if (whiteList.indexOf(from.path) !== -1) {
            switchPath(next);
          } else {
            next({ ...to, replace: true });
          }
        } catch (e) {
          removeToken();
          error(e || "Has Error");
          next("/login");
        }
      }
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) {
      next();
    } else {
      next("/login");
    }
  }
})
router.afterEach(() => {})
