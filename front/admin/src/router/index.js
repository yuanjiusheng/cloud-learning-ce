import {createRouter, createWebHistory} from "vue-router";
import Layout from "../components/Layout.vue";
import Login from "../views/login/Index"
import workWeChatLogin from "../views/login/workWeChat"
import dingTalkLogin from "../views/login/dingTalk"
import Index from "../views/home/Index"
import memberList from "../views/member/list/index"
// import memberLevel from "../views/member/level"
import lessonCategory from "../views/learn/category/index"
import lesson from "../views/learn/lesson/index"
import lessonEdit from "../views/learn/lesson/edit"
import commentList from "../views/comment/index"
import commentSensitiveWord from "../views/comment/sensitiveWord/index"
import organizationalUser from "../views/organizational/user/index"
import authAuthority from "../views/auth/authority/index"
import authRole from "../views/auth/role/index"
import SettingCarousel from "../views/setting/carousel/index"
import SettingAgreement from "../views/setting/agreement/index"

export const routes = [
  {
    path: "/login",
    name: "login",
    component: Login,
    hidden: true
  },
  {
    path: "/work-we-chat",
    name: "workWeChatLogin",
    component: workWeChatLogin,
    hidden: true
  },
  {
    path: "/ding-talk",
    name: "dingTalk",
    component: dingTalkLogin,
    hidden: true
  },
  {
    path: "/",
    name: "Home",
    component: Layout,
    redirect: "/index",
    meta: { title: "仪表盘", icon: "el-icon-odometer" }
  },
  {
    path: "/index",
    name: "indexLayout",
    component: Layout,
    hidden: true,
    children: [{
      path: "",
      name: "index",
      component: Index
    }]
  }
];

export const asyncRoutes = [
  {
    path: "/member",
    component: Layout,
    redirect: "/member/list",
    meta: { title: "会员管理", icon: "el-icon-user", authorities: ["member"] },
    children: [
      {
        path: "list",
        name: "memberList",
        component: memberList,
        meta: { title: "会员管理", icon: "el-icon-female", authorities: ["member_list"] },
        children: [{
          path: "edit",
          name: "memberEdit",
          component: () => import("../views/home/Index"),
          meta: { title: "编辑 - 会员管理", icon: "el-icon-basketball", activeMenu: "/member/list" },
          hidden: true
        }]
      },
      // {
      //   path: "level",
      //   name: "memberLevel",
      //   component: memberLevel,
      //   meta: { title: "会员等级", icon: "el-icon-s-data", authorities: ["member_level"] }
      // }
    ]
  },
  {
    path: "/learn",
    component: Layout,
    redirect: "/learn/lesson",
    meta: { title: "学习培训", icon: "el-icon-notebook-1", authorities: ["learning"] },
    children: [
      {
        path: "lesson",
        name: "learnLesson",
        component: lesson,
        meta: { title: "课程管理", icon: "el-icon-notebook-2", authorities: ["learning_list"] },
        children: [
          {
            path: "edit",
            name: "learnLessonEdit",
            component: lessonEdit,
            meta: { title: "新增/编辑 - 课程管理", icon: "el-icon-basketball", activeMenu: "/learn/lesson" },
            hidden: true
          }
        ]
      },
      {
        path: "category",
        name: "learnCategory",
        component: lessonCategory,
        meta: { title: "课程分类", icon: "el-icon-folder", authorities: ["learning_category"] },
      },
    ]
  },
  {
    name: "comment",
    path: "/comment",
    component: Layout,
    meta: { title: "评论管理", icon: "el-icon-chat-dot-square", authorities: ["comment"] },
    redirect: "/comment/list",
    children: [
      {
        path: "list",
        name: "commentList",
        component: commentList,
        meta: { title: "评论列表", icon: "el-icon-tickets", authorities: ["comment_list"] }
      },
      {
        path: "sensitive-word",
        name: "commentSensitiveSetting",
        component: commentSensitiveWord,
        meta: { title: "敏感词管理", icon: "el-icon-circle-close", authorities: ["comment_sensitive_setting"] }
      }
    ]
  },
  {
    name: "organizational",
    path: "/organizational",
    component: Layout,
    meta: { title: "组织架构", icon: "el-icon-office-building", authorities: ["organizational"] },
    redirect: "/organizational/user",
    children: [
      {
        path: "user",
        name: "organizationalUser",
        component: organizationalUser,
        meta: { title: "用户管理", icon: "el-icon-user-solid", authorities: ["organizational_user"] }
      }
    ]
  },
  {
    name: "auth",
    path: "/auth",
    component: Layout,
    meta: { title: "权限认证", icon: "el-icon-unlock", authorities: ["authority"] },
    redirect: "/auth/authority",
    children: [
      {
        path: "role",
        name: "authorityRole",
        component: authRole,
        meta: { title: "角色管理", icon: "el-icon-location-outline", authorities: ["authority_role"] }
      },
      {
        path: "authority",
        name: "authorityAuthority",
        component: authAuthority,
        meta: { title: "权限列表", icon: "el-icon-lock", authorities: ["authority_authority"] }
      }
    ]
  },
  {
    path: "/setting",
    name: "setting",
    component: Layout,
    meta: { title: "系统设置", icon: "el-icon-setting", authorities: ["setting"] },
    redirect: "/setting/carousel",
    children: [
      {
        path: "carousel",
        name: "settingCarousel",
        component: SettingCarousel,
        meta: { title: "首页轮播图", icon: "el-icon-picture-outline-round", authorities: ["setting_carousel"] }
      },
      {
        path: "agreement",
        name: "settingAgreement",
        component: SettingAgreement,
        meta: { title: "协议管理", icon: "el-icon-document", authorities: ["setting_agreement"] }
      }
    ]
  }
]

let routerOptions = {
  history: createWebHistory(process.env.BASE_URL),
  routes
};
let router = createRouter(routerOptions);
export default router;
