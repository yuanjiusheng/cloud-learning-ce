import { get, post, put, del } from "../../util/requestUtils"

export function findList(params, success) {
  return get("/learn/lesson/list", params, success)
}

export function saveBaseInfo(params, success) {
  return post("/learn/lesson", params, success)
}

export function updateBaseInfo(params, success) {
  return put("/learn/lesson", params, success)
}

export function getBaseInfo(id, success) {
  return get("/learn/lesson", { id: id }, success)
}

export function publishLesson(params, success) {
  return put("/learn/lesson/publish", params, success)
}

export function unPublishLesson(params, success) {
  return put("/learn/lesson/un-publish", params, success)
}

export function saveLessonChapter(params, success) {
  return post("/learn/lesson/chapter", params, success)
}

export function updateLessonChapter(params, success) {
  return put("/learn/lesson/chapter", params, success)
}

export function deleteLessonChapter(params, success) {
  return del("/learn/lesson/chapter", params, success)
}

export function getLessonChapterList(params, success) {
  return get("/learn/lesson/chapter/list", params, success)
}

export function saveLessonChapterSection(params, success) {
  return post("/learn/lesson/chapter-section", params, success)
}

export function updateLessonChapterSection(params, success) {
  return put("/learn/lesson/chapter-section", params, success)
}

export function deleteLessonChapterSection(params, success) {
  return del("/learn/lesson/chapter-section", params, success)
}

export function getHomeworkRecordList(params, success) {
  return get("/learn/homework/record/list", params, success)
}

export function passApproval(data, success) {
  return put("/learn/homework/record/approval/pass", data, success)
}

export function rejectApproval(data, success) {
  return put("/learn/homework/record/approval/reject", data, success)
}
