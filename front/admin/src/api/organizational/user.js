import { get } from "../../util/requestUtils";

export function getUserInfo(mobile, callback) {
  const params = { mobile: mobile };
  return get("/user-center/auth-api/by-mobile", params, callback);
}

export function getUserList(params, callback) {
  return get("/user-center/list", params, callback);
}

