import { get, post, put, del } from "../../util/requestUtils"

// 新增敏感词
export function saveSensitiveWord(data, success) {
  return post("/comment/sensitive-word", data, success)
}

// 更新敏感词
export function updateSensitiveWord(data, success) {
  return put("/comment/sensitive-word", data, success)
}

// 新增敏感词
export function removeSensitiveWord(data, success) {
  return del("/comment/sensitive-word", data, success)
}

export function getSensitiveWordList(param, success) {
  return get("/comment/sensitive-word/list", param, success)
}
