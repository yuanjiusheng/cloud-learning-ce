import { get, post, put } from "../../util/requestUtils"

// 点赞
export function saveLike(data, success) {
  return post("/comment/auth-api/like", data, success)
}

// 取消赞/点赞
export function updateLike(data, success) {
  return put("/comment/auth-api/like", data, success)
}

export function getMemberLikeList(param, success) {
  return get("/comment/auth-api/like/list", param, success)
}

export function getLikeCountList(param, success) {
  return get("/comment/public-api/like/count", param, success)
}

export function like(item, type, success) {
  if (item["like"] && item["like"].id) {
    item["like"].status = !item["like"].status
    updateLike(item["like"], res => {
      console.log(res)
      if (!item.likeCount) {
        item.likeCount = 0
      }
      if (!res.status) {
        item.likeCount = item.likeCount - 1
      } else {
        item.likeCount = item.likeCount + 1
      }
      if (item.likeCount === 0) {
        item.likeCount = ""
      }
      const p = {
        like: item.like,
        likeCount: item.likeCount
      }
      success && success(p)
    }).then(() => {})
  } else {
    const p = { topicId: item.id, topicType: type }
    saveLike(p, res => {
      item.like = res
      if (!item.likeCount) {
        item.likeCount = 0
      }
      item.likeCount = item.likeCount + 1
      const p = {
        like: res,
        likeCount: item.likeCount
      }
      success && success(p)
    }).then(() => {})
  }
}
