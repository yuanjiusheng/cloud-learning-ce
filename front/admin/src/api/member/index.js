import { get, post, put, del } from "../../util/requestUtils"

export function getMemberList(param, success) {
  return get("/member/list", param, success)
}

export function addMemberLevel(data, success) {
  return post("/member/level", data, success)
}

export function editMemberLevel(data, success) {
  return put("/member/level", data, success)
}

export function getMemberLevel(id, success) {
  return get("/member/level", { id: id }, success)
}

export function listMemberLevel() {
  return "member/level/list"
}

export function removeMemberLevel(id, success) {
  const data = { id: id }
  return del("/member/level", data, success)
}

