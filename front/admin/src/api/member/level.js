import { get, put, post } from "../../util/requestUtils"

export function findList(params, success) {
  return get("/member/level/list", params, success)
}

export function getLevel(id, success) {
  return get("/member/public-api/level", {id: id}, success)
}

export function updateLevel(data, success) {
  return put("/member/level", data, success)
}

export function saveLevel(data, success) {
  return post("/member/level", data, success)
}

