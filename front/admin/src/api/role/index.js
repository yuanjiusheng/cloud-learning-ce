import {get, post, put, del} from "../../util/requestUtils"

export function getRoleList(callback) {
  return get("/auth/role/list", {}, callback);
}
// 获取角色列表
export function getRolePageList(params, callback) {
  return get("/auth/role/page/list", params, callback);
}
// 保存角色
export function saveRole(data, callback) {
  return post("/auth/role", data, callback);
}
// 更新角色
export function updateRole(data, callback) {
  return put("/auth/role", data, callback)
}
// 删除角色
export function deleteRole(id, callback) {
  return del("/auth/role", {id: id}, callback)
}
// 获取角色权限列表
export function getRoleAuthorityList(id, callback) {
  return get("/auth/role/authority/list", {id: id}, callback);
}
// 修改角色权限列表
export function saveOrUpdateRoleAuthorityList(data, callback) {
  return put("/auth/role/authority/update", data, callback)
}
// 获取用户的角色列表
export function getUserRoleList(userId, callback) {
  return get("/auth/role/user/list", {userId: userId}, callback);
}
// 更新用户的角色列表
export function updateUserRole(data, callback) {
  return put("/auth/role/user/list", data, callback)
}
