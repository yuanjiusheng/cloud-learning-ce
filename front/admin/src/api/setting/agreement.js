import { get, post, put } from "../../util/requestUtils";

// 保存
export function saveAgreement(data, successCallback) {
  return post("/setting/agreement", data, successCallback)
}

// 查询
export function getAgreement(params, successCallback) {
  return get("/setting/public-api/agreement", params, successCallback)
}

// 修改
export function putAgreement(data, successCallback) {
  return put("/setting/agreement", data, successCallback)
}

// 列表
export function getAgreementList(params, successCallback) {
  return get("/setting/agreement/page", params, successCallback)
}
