import { get, post } from "../../util/requestUtils";

// 保存首页轮播图
export function saveCarousel(params, successCallback) {
  return post("/setting/carousel", params, successCallback)
}

// 查询首页轮播图
export function getCarousel(params, successCallback) {
  return get("/setting/public-api/carousel", params, successCallback)
}

