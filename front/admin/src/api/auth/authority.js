import {get} from "../../util/requestUtils";

export function getAuthorityList(params, callback) {
  return get("/auth/authorities", params, callback)
}

export function getAuthorityTree(callback) {
  return get("/auth/authorities/tree", {}, callback)
}
