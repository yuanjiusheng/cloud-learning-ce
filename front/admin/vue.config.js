const path = require("path");
module.exports = {
  // 打包输出目录
  outputDir: "dist",
  // 打包静态文件输出目录
  assetsDir: "static",
  // 路由模式为history不能为./ 或者不填
  publicPath: "/",
  devServer: {
    open: true,
    // 端口
    port: 5558,
    overlay: {
      warnings: false,
      errors: true
    },
    // 代理
    proxy: {
      // change xxx-api/login => mock/login
      // detail: https://cli.vuejs.org/config/#devserver-proxy
      [process.env.VUE_APP_BASE_API]: {
        target: "https://api.chawind.com",
        // target: "http://localhost:5500",
        // 跨域访问设置，true代表跨域
        changeOrigin: true,
        // proxy web sockets
        ws: true,
        // false为http访问，true为https访问
        secure: false,
        pathRewrite: {
          ["^" + process.env.VUE_APP_BASE_API]: ""
        }
      }
    },
    disableHostCheck: true
  },
  configureWebpack: {
    // provide the app's title in webpack's name field, so that it can be accessed in index.html to inject the correct title.
    name: "知道-在线学习平台",
    devtool: "source-map",
    resolve: {
      alias: {
        "@": path.join(__dirname, "src")
      }
    }
  },
  chainWebpack: config => {
    // svg矢量图解析
    const svgRule = config.module.rule("svg");
    svgRule.uses.clear();
    svgRule
      .test(/\.svg$/)
      .include.add(path.resolve(__dirname, "src/assets/svg"))
      .end()
      .use("svg-sprite-loader")
      .loader("svg-sprite-loader")
      .options({
        symbolId: "icon-[name]"
      });
    const fileRule = config.module.rule("file");
    fileRule.uses.clear();
    fileRule
      .test(/\.svg$/)
      .exclude.add(path.resolve(__dirname, "src/assets/svg"))
      .end()
      .use("file-loader")
      .loader("file-loader");
  },
  css: {
    loaderOptions: {
      scss: {
        additionalData: "@import '~@/plugins/variables.scss';"
      }
    }
  }
};
