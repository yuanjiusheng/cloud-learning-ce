import strongUtils from "../util/storageUtils";
const currentUserKey = "cloud-learning-member";

export function setUser(data) {
  return strongUtils.setJson(currentUserKey, data)
}

export function getUser() {
  const user = strongUtils.getJson(currentUserKey)
  if (user) {
    return user
  }
  return undefined
}

export function deleteUser() {
  strongUtils.remove(currentUserKey);
}
