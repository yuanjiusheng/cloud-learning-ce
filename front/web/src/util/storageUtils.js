Storage.prototype.setExpire = (key, value, expire) =>{
  let obj = {
    data:value,
    time:Date.now(),
    expire:expire
  };
  localStorage.setItem(key, JSON.stringify(obj));
}
Storage.prototype.getExpire = key =>{
  let val =localStorage.getItem(key);
  if(!val){
    return val;
  }
  val = JSON.parse(val);
  if(Date.now() - val.time > val.expire){
    localStorage.removeItem(key);
    return null;
  }
  return val.data;
}
const storage = {
  set(key, value) {
    localStorage.setItem(key, value);
  },
  get(key) {
    return localStorage.getItem(key);
  },
  remove(key) {
    localStorage.removeItem(key);
  },
  setJson(key, value) {
    this.set(key, JSON.stringify(value));
  },
  getJson(key) {
    const value = this.get(key);
    if (value) {
      return JSON.parse(value);
    }
    return undefined;
  },
  setJsonExpire(key, value, expire) {
    localStorage.setExpire(key, JSON.stringify(value), expire);
  },
  getJsonExpire(key) {
    const value = localStorage.getExpire(key);
    if (value) {
      return JSON.parse(value);
    }
    return undefined;
  }
}
export default storage;
