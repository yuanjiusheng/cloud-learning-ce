import routerGuard from "./routerGuardLoadScript"

const install = function(app) {
  app.directive("routerGuard", routerGuard)
}

if (window.app) {
  window["routerGuard"] = routerGuard
  window.app.use(install);
}

routerGuard.install = install
export default routerGuard
