import LoginComponent from "./Login.vue"

const Login = {
  install: function(Vue) {
    Vue.component("LoginIndex", LoginComponent)
  }
}

export default Login
