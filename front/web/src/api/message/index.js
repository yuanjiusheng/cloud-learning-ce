import { get, post } from "../../util/requestUtils"

export function getMemberList(param, success) {
  return get("/message/auth-api/private-letter/member/list", param, success)
}

export function getLetterList(param, success) {
  return get("/message/auth-api/private-letter/list", param, success)
}

export function sendPrivateLetter(data, success) {
  return post("/message/auth-api/private-letter", data, success)
}

export function getLetterMember(param, success) {
  return get("/message/auth-api/private-letter/member", param, success)
}

export function getNewLetterList(param, success) {
  return get("/message/auth-api/private-letter/new/list", param, success)
}
