import { del } from "../../util/requestUtils"

// 删除文件
export function deleteFile(path, successCallback) {
  return del("/oss/file", { path: path }, successCallback)
}

