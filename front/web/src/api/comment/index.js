import { get, post, del } from "../../util/requestUtils"

// 发表评论
export function saveComment(data, success) {
  return post("/comment/auth-api/comment", data, success)
}

// 删除评论
export function deleteComment(data, success) {
  return del("/comment/auth-api/comment", data, success)
}

// 评论列表
export function getCommentList(params, success) {
  return get("/comment/public-api/comment/list", params, success)
}

// 获取当前用户的评论列表
export function getCurrentMemberCommentList(params, success) {
  return get("/comment/auth-api/current-member/comment/list", params, success)
}

// 发表回复
export function saveReplyComment(data, success) {
  return post("/comment/auth-api/reply/comment", data, success)
}

// 删除回复评论
export function deleteReplyComment(data, success) {
  return del("/comment/auth-api/reply/comment", data, success)
}

