import { get, post, del } from "../../util/requestUtils"

export function saveFavorite(data, success) {
  return post("/comment/auth-api/favorite", data, success)
}

export function deleteFavorite(data, success) {
  return del("/comment/auth-api/favorite", data, success)
}

export function getMemberFavoriteList(param, success) {
  return get("/comment/auth-api/favorite/list", param, success)
}

export function getFavoriteCountList(param, success) {
  return get("/comment/public-api/favorite/count", param, success)
}

export function favorite(item, type, success) {
  if (item["favorite"] && item["favorite"].id) {
    deleteFavorite({ id: item["favorite"].id }, () => {
      item.favorite = {}
      if (!item.favoriteCount) {
        item.favoriteCount = 0
      }
      item.favoriteCount = item.favoriteCount - 1
      if (item.favoriteCount === 0) {
        item.favoriteCount = ""
      }
      const p = {
        favorite: item.favorite,
        favoriteCount: item.favoriteCount
      }
      success && success(p)
    }).then(() => {})
  } else {
    saveFavorite({ topicId: item.id, topicType: type }, res => {
      item.favorite = res
      if (!item.favoriteCount) {
        item.favoriteCount = 0
      }
      item.favoriteCount = item.favoriteCount + 1
      const p = {
        favorite: res,
        favoriteCount: item.favoriteCount
      }
      success && success(p)
    }).then(() => {})
  }
}
