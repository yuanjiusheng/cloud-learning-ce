import { get, post, put } from "../../util/requestUtils"

export function getLesson(id, success) {
  return get("/learn/public-api/lesson", { id: id }, success)
}

export function getLessonChapterList(params, success) {
  return get("/learn/public-api/lesson/chapter/list", params, success)
}

export function getLessonList(params, success) {
  return get("/learn/public-api/lesson/list", params, success)
}

export function getFavoriteLessonList(params, success) {
  return get("/learn/auth-api/lesson/favorite/list", params, success)
}

export function saveSignUp(data, success) {
  return post("/learn/auth-api/sign-up", data, success)
}

export function saveRecord(data, success) {
  return post("/learn/auth-api/record", data, success)
}

export function updateRecord(data, success) {
  return put("/learn/auth-api/record", data, success)
}

export function getRecordLessonList(params, success) {
  return get("/learn/auth-api/lesson/member/learn/list", params, success)
}

export function getHomework(params, success) {
  return get("/learn/auth-api/homework/record", params, success)
}

export function updateHomework(params, success) {
  return put("/learn/auth-api/homework/record", params, success)
}

export function saveHomework(params, success) {
  return post("/learn/auth-api/homework/record", params, success)
}

// 报名信息
export function learnSignUp(params, success) {
  return get("/learn/public-api/sign-up", params, success)
}
