import { get } from "../../util/requestUtils"

// 查询首页轮播图
export function getCarousel(params, successCallback) {
  return get("/setting/public-api/carousel", params, successCallback)
}

