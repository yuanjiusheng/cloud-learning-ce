import { get } from "../../util/requestUtils"

// 查询
export function getAgreement(params, successCallback) {
  return get("/setting/public-api/agreement", params, successCallback)
}
