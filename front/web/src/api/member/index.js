import { get, post, put } from "../../util/requestUtils"
import {getUser, setUser} from "../../util/userUtils";
import {getCurrentUser} from "../login";
import {setAuthorities} from "../../util/authorityUtils";

export function registerMember(data, callback) {
  return post("/member/public-api/register", data, callback)
}

export function registerMemberByMobile(data, callback) {
  return post("/member/public-api/register/mobile", data, callback)
}

export function updateAvatar(data, success) {
  put("/member/auth-api/update/avatar", data, success)
}

export function updateName(data, success) {
  put("/member/auth-api/update/name", data, success)
}

export function updateMobile(data, success) {
  put("/member/auth-api/update/mobile", data, success)
}

export function updateEmail(data, success) {
  put("/member/auth-api/update/email", data, success)
}

export function updatePassword(data, success) {
  put("/member/auth-api/update/password", data, success)
}

export function getMemberInfo(success) {
  const m = getUser()
  if (!m) {
    return;
  }
  const params = { mobile: m.mobile || m.username || m.email }
  return get("/member/auth-api/by-mobile", params, success)
}

export function getMemberByMobile(mobile, callback) {
  const params = { mobile: mobile };
  return get("/member/auth-api/by-mobile", params, callback);
}

export function getMember() {
  return new Promise((resolve, reject) => {
    getCurrentUser(res => {
      // 保存权限信息
      setAuthorities(res);
      // 获取用户信息
      getMemberByMobile(res.username, user => {
        setUser(user);
        return resolve(user);
      }).catch(e => {
        reject(e)
      });
    }).catch(e => {
      reject(e)
    })
  });
}

export function getAuthMemberList(params, callback) {
  return get("/member/auth-api/list", params, callback);
}
