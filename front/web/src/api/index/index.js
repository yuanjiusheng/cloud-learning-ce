import { get } from "../../util/requestUtils"

// 查询推荐课程
export function getRecommendLesson(params, success) {
  return get("/learn/public-api/lesson/recommend", params, success)
}

// 查询分类热门课程
export function getHotLesson(params, success) {
  return get("/learn/public-api/lesson/hot", params, success)
}

import * as lessonCategoryApi from "../../api/learn/category";
import storageUtils from "../../util/storageUtils";
export async function getCategory(success) {
  // 10分钟过期
  const expire = 600000
  if (storageUtils.getJsonExpire("learn")) {
    const toTree = storageUtils.getJsonExpire("learn");
    success && success({value: 0, type: "learn", label: "课程", children: toTree})
    console.log("获取缓存")
  } else {
    // 获取缓存的目录
    await lessonCategoryApi.findCategoryList(0, true, (res) => {
      const toTree = lessonCategoryApi.toTree(res);
      storageUtils.setJsonExpire("learn", toTree, expire)
      success && success({value: 0, type: "learn", label: "课程", children: toTree})
      console.log("重新拿了")
    })
  }
}
