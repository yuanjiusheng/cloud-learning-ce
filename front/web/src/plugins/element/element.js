import ElementPlus from "element-plus"
import locale from "element-plus/lib/locale/lang/zh-cn"
import "./element-variables.scss"
// import "element-plus/lib/theme-chalk/display.css";

export default (app) => {
  app.use(ElementPlus, { locale });
  return ElementPlus;
}
