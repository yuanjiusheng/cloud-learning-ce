import {createRouter, createWebHistory} from "vue-router";
import Layout from "../components/Layout.vue";

export const routes = [
  {
    path: "/",
    name: "Home",
    component: Layout,
    redirect: "/index",
  },
  {
    path: "/index",
    name: "indexLayout",
    component: Layout,
    children: [{
      path: "",
      name: "index",
      component:  () => import("../views/learn/index")
    }]
  },
  {
    path: "/register",
    component: Layout,
    children: [
      {
        path: "",
        name: "register",
        component: () => import("../components/Register"),
      }
    ]
  },
  {
    path: "/agreement/:type",
    name: "agreement",
    component: () => import("../views/agreement/index"),
  },
  {
    path: "/search",
    name: "search",
    component: Layout,
    children: [
      {
        path: "list",
        name: "searchList",
        component: () => import("../views/search/index"),
      }
    ]
  },
  {
    path: "/learn",
    name: "learn",
    component: Layout,
    children: [
      {
        path: "detail",
        name: "learnDetail",
        component: () => import("../views/learn/detail/index"),
      },
      {
        path: "list",
        name: "learnList",
        component: () => import("../views/learn/list/index"),
      },
      {
        path: "",
        name: "learnIndex",
        redirect: "/learn/list",
      },
      {
        path: "certificate",
        name: "certificate",
        component: () => import("../views/learn/certificate/index"),
      }
    ]
  },
  {
    path: "/member",
    name: "member",
    component: Layout,
    children: [
      {
        path: "message",
        name: "memberMessage",
        component: () => import("../views/member/message/index"),
      },
      {
        path: "personal",
        name: "memberPersonal",
        component: () => import("../views/member/personal/index"),
      },
      {
        path: "favorites",
        name: "memberFavorites",
        component: () => import("../views/member/favorites/index"),
      },
      {
        path: "learn-record",
        name: "memberLearnRecord",
        component: () => import("../views/member/learn-record/index"),
      },
      {
        path: "setting",
        name: "memberSetting",
        component: () => import("../views/member/setting/index"),
      },
      {
        path: "comment",
        name: "memberComment",
        component: () => import("../views/member/comment/index"),
      }
    ]
  }
];

export const asyncRoutes = [
]

let routerOptions = {
  history: createWebHistory(process.env.BASE_URL),
  routes
};
let router = createRouter(routerOptions);
export default router;
