import {getToken, refreshToken, removeToken} from "../util/tokenUtils";
import {getUser} from "../util/userUtils";
import {error} from "../util/tipsUtils";
import router from "./index"

router.beforeEach(async (to, from, next) => {
  // document.title = getPageTitle(to.meta.title)
  refreshToken()
  const hasToken = getToken()
  if (hasToken) {
    if (to.path === "/login") {
      next();
    } else {
      const userInfo = getUser()
      if (userInfo) {
        next();
      } else {
        try {
          next();
        } catch (e) {
          removeToken();
          error(e || "Has Error");
          next();
        }
      }
    }
  } else {
    next();
  }
})
router.afterEach(() => {})
