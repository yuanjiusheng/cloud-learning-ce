import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import "./router/guard";
import store from "./store";
import elementPlus from "./plugins/element/element";
import Login from "./components/Login";
const app = createApp(App)
//将其挂载到全局
const TcPlayer = window.TcPlayer
app.config.globalProperties.TcPlayer = TcPlayer
export default app.use(store).use(router).use(elementPlus).use(Login).mount("#app")
// export default createApp(App)
//   .use(store)
//   .use(router)
//   .use(elementPlus)
//   .use(Login)
//   .mount("#app");
